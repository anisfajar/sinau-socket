import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [{
            path: "/kritik",
            name: "kritik",
            component: () =>
                import ("./components/kritik/KritikList")
        },
        {
            path: "/kritik/:id",
            name: "kritik-details",
            component: () =>
                import ("./components/kritik/Kritik")
        },
        {
            path: "/",
            name: "beranda",
            component: () =>
                import ("./components/Beranda")
        },
    ]
});