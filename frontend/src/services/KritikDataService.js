import http from "../http-common";

class KritikDataService {
    getAll() {
        return http.get("/kritik");
    }

    get(id) {
        return http.get(`/kritik/${id}`);
    }

    create(data) {
        return http.post("/kritik", data);
    }

    update(id, data) {
        return http.put(`/kritik/${id}`, data);
    }

    findByJenisKritikan(jenis) {
        return http.get(`/kritik?jenis_kritikan=${jenis}`);
    }
}

export default new KritikDataService();