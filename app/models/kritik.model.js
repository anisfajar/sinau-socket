module.exports = (sequelize, Sequelize) => {
  const Kritikan = sequelize.define('kritik', {
    nama: {
      type: Sequelize.STRING,
    },
    alamat: {
      type: Sequelize.STRING,
    },
    nohp: {
      type: Sequelize.STRING,
    },
    jenis_kritikan: {
      type: Sequelize.STRING,
    },
    isi_kritikan: {
      type: Sequelize.STRING,
    },
    is_accept: {
      type: Sequelize.BOOLEAN,
    },
  }, {
    updatedAt: 'last_update',
    createdAt: 'input_time',
  });

  return Kritikan;
};
