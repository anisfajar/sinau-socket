module.exports = (sequelize, Sequelize) => {
  const Saran = sequelize.define('saran', {
    nama: {
      type: Sequelize.STRING,
    },
    alamat: {
      type: Sequelize.STRING,
    },
    nohp: {
      type: Sequelize.STRING,
    },
    jenis_saran: {
      type: Sequelize.STRING,
    },
    isi_saran: {
      type: Sequelize.STRING,
    },
    is_accept: {
      type: Sequelize.BOOLEAN,
    },
  }, {
    updatedAt: 'last_update',
    createdAt: 'input_time',
  });

  return Saran;
};
