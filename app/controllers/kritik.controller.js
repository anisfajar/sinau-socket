const db = require('../models');

const Kritik = db.kritiks;
const { Op } = db.Sequelize;

// Create and Save a new kritik
exports.create = (req, res) => {
    // Validate request
    if (!req.body.nama) {
        res.status(400).send({
            message: 'Nama tidak boleh kosong!',
        });
        return;
    }
    if (!req.body.alamat) {
        res.status(400).send({
            message: 'Alamat tidak boleh kosong!',
            status: 400,
        });
    } else if (!req.body.nohp) {
        res.status(400).send({
            message: 'No. Telp tidak boleh kosong!',
            status: 400,
        });
    } else if (!req.body.jenis_kritikan) {
        res.status(400).send({
            message: 'Jenis kritikan tidak boleh kosong!',
            status: 400,
        });
    } else if (!req.body.isi_kritikan) {
        res.status(400).send({
            message: 'Isi kritikan tidak boleh kosong!',
            status: 400,
        });
    } else {
        // Create a kritik
        const kritik = {
            nama: req.body.nama,
            alamat: req.body.alamat,
            nohp: req.body.nohp,
            jenis_kritikan: req.body.jenis_kritikan,
            isi_kritikan: req.body.isi_kritikan,
            is_accept: req.body.is_accept ? req.body.is_accept : false,
        };

        // Save kritik
        Kritik.create(kritik)
            .then((data) => {
                req.io.emit('newData', { data: data.dataValues })
                res.send({ message: 'Kritik anda telah tersampaikan', status: 200 });
            })
            .catch((err) => {
                res.status(500).send({
                    message: err.message || 'Error saat simpan kritik.',
                });
            });
    }
};

// Retrieve all kritik from the database.
exports.findAll = (req, res) => {
    const { jenis_kritikan } = req.query;
    const condition = jenis_kritikan ? {
        jenis_kritikan: {
            [Op.like]: `%${jenis_kritikan}%`,
        },
    } : null;

    Kritik.findAll({ where: condition })
        .then((data) => {
            req.io.emit('newData', { data: data.dataValues })
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || 'Error saat ambil data kritik.',
            });
        });
};

// Find a single kritik with an id
exports.findOne = (req, res) => {
    const { id } = req.params;

    Kritik.findByPk(id)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message: `Error ambil data kritik dengan ID =${id}`,
            });
        });
};

// Update a kritik by the id in the request
exports.update = (req, res) => {
    const { id } = req.params;

    Kritik.update(req.body, {
            where: { id },
        })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: 'Kritik berhasil di update.',
                });
            } else {
                res.send({
                    message: `Tidak bisa edit kritik dengan id=${id}. Mungkin kritik tidak ketemu atau req.body kosong!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: `Gagal update kritik dengan id=${id}`,
            });
        });
};

// Delete a kritik with the specified id in the request
exports.delete = (req, res) => {
    const { id } = req.params;

    Kritik.destroy({
            where: { id },
        })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: 'Kritik berhasil di hapus!',
                });
            } else {
                res.send({
                    message: `Gagal hapus kritik dengan ID=${id}. Mungkin kritik tidak ketemu !`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: `Gagal hapus kritik dengan ID=${id}`,
            });
        });
};

// Delete all kritik from the database.
exports.deleteAll = (req, res) => {
    Kritik.destroy({
            where: {},
            truncate: false,
        })
        .then((nums) => {
            res.send({ message: `${nums} Kritik sukses di hapus!` });
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || 'Gagal hapus semua data kritik.',
            });
        });
};

// Find all kritik accept
exports.findAllAccept = (req, res) => {
    Kritik.findAll({ where: { is_accept: true } })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || 'Gagal ambil data kritik.',
            });
        });
};