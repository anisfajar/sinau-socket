const db = require('../models');

const Saran = db.sarans;
const { Op } = db.Sequelize;

// Create and Save a new kritik
exports.create = (req, res) => {
  // Validate request
  if (!req.body.nama) {
    res.status(400).send({
      message: 'Nama tidak boleh kosong!',
    });
    return;
  } if (!req.body.alamat) {
    res.status(400).send({
      message: 'Alamat tidak boleh kosong!',
      status: 400,
    });
  } else if (!req.body.nohp) {
    res.status(400).send({
      message: 'No. Telp tidak boleh kosong!',
      status: 400,
    });
  } else if (!req.body.jenis_saran) {
    res.status(400).send({
      message: 'Jenis saran tidak boleh kosong!',
      status: 400,
    });
  } else if (!req.body.isi_saran) {
    res.status(400).send({
      message: 'Isi saran tidak boleh kosong!',
      status: 400,
    });
  } else {
    // Create a saran
    const saran = {
      nama: req.body.nama,
      alamat: req.body.alamat,
      nohp: req.body.nohp,
      jenis_saran: req.body.jenis_saran,
      isi_saran: req.body.isi_saran,
      is_accept: req.body.is_accept ? req.body.is_accept : false,
    };

    // Save saran
    Saran.create(saran)
      .then((data) => {
        res.send({ message: 'Saran anda telah tersampaikan', status: 200 });
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || 'Error saat simpan saran.',
        });
      });
  }
};

// get semua saran
exports.findAll = (req, res) => {
  const { isi_saran } = req.query;
  const condition = isi_saran ? {
    isi_saran: {
      [Op.like]: `%${isi_saran}%`,
    },
  } : null;

  Saran.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Error saat ambil data saran.',
      });
    });
};

// cari saran berdasarkan ID
exports.findOne = (req, res) => {
  const { id } = req.params;

  Saran.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error ambil data saran dengan ID =${id}`,
      });
    });
};

// update saran berdasarkan ID
exports.update = (req, res) => {
  const { id } = req.params;

  Saran.update(req.body, {
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Saran berhasil di update.',
        });
      } else {
        res.send({
          message: `Tidak bisa edit saran dengan id=${id}. Mungkin saran tidak ketemu atau req.body kosong!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Gagal update kritik dengan id=${id}`,
      });
    });
};

// hapus saran berdasarkan ID
exports.delete = (req, res) => {
  const { id } = req.params;

  Saran.destroy({
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Saran berhasil di hapus!',
        });
      } else {
        res.send({
          message: `Gagal hapus saran dengan ID=${id}. Mungkin saran tidak ketemu !`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Gagal hapus kritik dengan ID=${id}`,
      });
    });
};

// Delete all kritik from the database.
exports.deleteAll = (req, res) => {
  Saran.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} Saran sukses di hapus!` });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Gagal hapus semua data saran.',
      });
    });
};

// Find all kritik accept
exports.findAllAccept = (req, res) => {
  Saran.findAll({ where: { is_accept: true } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Gagal ambil data saran.',
      });
    });
};
