const router = require('express').Router();
const saran = require('../controllers/saran.controller.js');

module.exports = (app) => {
  // Create a new saran
  router.post('/save', saran.create);

  // Retrieve all saran
  router.get('/', saran.findAll);

  // Retrieve all acc saran
  router.get('/acc', saran.findAllAccept);

  // Retrieve a single saran with id
  router.get('/:id', saran.findOne);

  // Update a saran with id
  router.put('/:id', saran.update);

  // Delete a saran with id
  router.delete('/:id', saran.delete);

  // Delete all saran
  router.delete('/', saran.deleteAll);

  app.use('/api/saran', router);
};
