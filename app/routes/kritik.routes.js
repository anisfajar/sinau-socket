const router = require('express').Router();
const kritik = require('../controllers/kritik.controller.js');

module.exports = (app) => {
    // save 
    router.post('/save', kritik.create);

    // get
    router.get('/', kritik.findAll);

    // Retrieve all acc kritik
    router.get('/acc', kritik.findAllAccept);

    // Retrieve a single kritik with id
    router.get('/:id', kritik.findOne);

    // Update a kritik with id
    router.put('/:id', kritik.update);

    // Delete a kritik with id
    router.delete('/:id', kritik.delete);

    // Delete all kritik
    router.delete('/', kritik.deleteAll);

    app.use('/api/kritik', router);
};