const http = require('http')
const app = require('express')()
const server = http.createServer(app)
const cors = require('cors')
const path = require('path')

const io = require('socket.io')(server, {
    cors: {
        origin: '*',
    },
})

const bodyParser = require('body-parser')
const db = require('./app/models')
db.sequelize.sync({ force: true }).then(() => {
    console.info('Drop and re-sync db process has been done.');
})
app.use(bodyParser.urlencoded({ extended: true }))

app.use(cors())

// parse application/json
app.use(bodyParser.json())

const ioMiddleware = (req, res, next) => {
    Object.assign(req, { io });
    next(); // next adalah object untuk menandakan eksekusi middleware ini telah seselah, lanjut ke middleware selanjutnya
}

app.use(ioMiddleware);

app.get('/', (req, res) => {
    res.send('<h1>Hello world</h1>')
})

require('./app/routes/kritik.routes')(app)
require('./app/routes/saran.routes')(app)

io.on('connection', (socket) => {
    console.log('a user connected')
})

server.listen(3030, () => {
    console.log('listening on *:3030')
})